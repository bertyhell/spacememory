import { GameUtils } from './game-utils';
import * as BABYLON from 'babylonjs';
import * as GUI from 'babylonjs-gui';
import { saveAs } from 'file-saver';
import * as _ from 'lodash';

export class Game {

  private _canvas: HTMLCanvasElement;
  private _engine: BABYLON.Engine;
  private _scene: BABYLON.Scene;
  private _camera: BABYLON.FreeCamera;
  private _light: BABYLON.Light;
  private _sharkMesh: BABYLON.AbstractMesh;
  private _sharkAnimationTime = 0;
  private _swim: boolean = false;
  private _txtCoordinates: { txtX: GUI.TextBlock, txtY: GUI.TextBlock, txtZ: GUI.TextBlock } = null;

  constructor(canvasElement: string) {
    // Create canvas and engine
    this._canvas = <HTMLCanvasElement>document.getElementById(canvasElement);
    this._engine = new BABYLON.Engine(this._canvas, true, { preserveDrawingBuffer: true, stencil: true });
  }

  /**
   * Creates the BABYLONJS Scene
   */
  createScene(): void {
    // create a basic BJS Scene object
    this._scene = new BABYLON.Scene(this._engine);
    // create a FreeCamera, and set its position to (x:0, y:5, z:-10)
    this._camera = new BABYLON.FreeCamera("Camera", new BABYLON.Vector3(0, 10, 0), this._scene);
    this._camera.attachControl(this._canvas, true);
    this._camera.inputs.addKeyboard();
    this._camera.inputs.addMouse();
    // create a basic light, aiming 0,1,0 - meaning, to the sky
    this._light = new BABYLON.HemisphericLight("light", new BABYLON.Vector3(0, 1, 0), this._scene);
    // create the skybox
    let skybox = GameUtils.createSkybox("skybox", "./assets/texture/skybox/TropicalSunnyDay", this._scene);
    // creates the sandy ground
    let ground = GameUtils.createGround(this._scene);

    // Our built-in 'sphere' shape.
    GameUtils.createWall(this._scene, {
      width: 100,
      height: 20,
      position: { x: 0, y: 10, z: -100 },
      rotation: { y: 180 },
      texture: './assets/texture/walls/agingkitv1_02.jpg'
    });
    GameUtils.createWall(this._scene, {
      width: 200,
      height: 20,
      position: { x: -50, y: 10, z: 0 },
      rotation: { y: 270 },
      texture: './assets/texture/walls/arroway.de_boards+02_d100.jpg'
    });
    GameUtils.createWall(this._scene, {
      width: 100,
      height: 20,
      position: { x: 0, y: 10, z: 100 },
      rotation: { y: 0 },
      texture: './assets/texture/walls/arroway.de_tiles-26_d100.jpg'
    });
    GameUtils.createWall(this._scene, {
      width: 200,
      height: 20,
      position: { x: 50, y: 10, z: 0 },
      rotation: { y: 90 },
      texture: './assets/texture/walls/mauerwerk_0017_c.jpg'
    });

    // creates the watermaterial and adds the relevant nodes to the renderlist
    // let waterMaterial = GameUtils.createWater(this._scene);
    // waterMaterial.addToRenderList(skybox);
    // waterMaterial.addToRenderList(ground);
    // // create a shark mesh from a .obj file
    // GameUtils.createShark(this._scene)
    //     .subscribe(sharkMesh => {
    //         this._sharkMesh = sharkMesh;
    //         this._sharkMesh.getChildren().forEach(
    //             mesh => {
    //                 waterMaterial.addToRenderList(mesh);
    //             }
    //         );
    //     });
    // // finally the new ui
    // let guiTexture = GameUtils.createGUI();
    //
    // // Button to start shark animation
    // GameUtils.createButtonSwim(guiTexture, "Start Swimming",
    //     (btn) => {
    //         let textControl = btn.children[0] as GUI.TextBlock;
    //         this._swim = !this._swim;
    //         if (this._swim) {
    //             textControl.text = "Stop Swimming";
    //         }
    //         else {
    //             this._sharkAnimationTime = 0;
    //             textControl.text = "Start Swimming";
    //         }
    //     });
    //
    // // Debug Text for Shark coordinates
    // this._txtCoordinates = GameUtils.createCoordinatesText(guiTexture);
    //
    // // Physics engine also works
    // let gravity = new BABYLON.Vector3(0, -0.9, 0);
    // this._scene.enablePhysics(gravity, new BABYLON.CannonJSPlugin());

    this.generateScreens();
  }

  /**
   * Starts the animation loop.
   */
  animate(): void {
    this._scene.registerBeforeRender(() => {
      let deltaTime: number = (1 / this._engine.getFps());
      this.animateShark(deltaTime);
    });

    // run the render loop
    this._engine.runRenderLoop(() => {
      this._scene.render();
    });

    // the canvas/window resize event handler
    window.addEventListener('resize', () => {
      this._engine.resize();
    });
  }

  animateShark(deltaTime: number): void {
    this.debugFirstMeshCoordinate(this._sharkMesh as BABYLON.Mesh);
    if (this._sharkMesh && this._swim) {
      this._sharkAnimationTime += 0.01;
      this._sharkMesh.getChildren().forEach(
          mesh => {
            let realMesh = <BABYLON.Mesh>mesh;
            let vertexData = BABYLON.VertexData.ExtractFromMesh(realMesh);
            let positions = vertexData.positions;
            let numberOfPoints = positions.length / 3;
            for (let i = 0; i < numberOfPoints; i++) {
              let vertex = new BABYLON.Vector3(positions[i * 3], positions[i * 3 + 1], positions[i * 3 + 2]);
              vertex.x += (Math.sin(0.15 * vertex.z + this._sharkAnimationTime * 4 - 1.6) * 0.05);
              positions[i * 3] = vertex.x;

            }
            vertexData.applyToMesh(mesh as BABYLON.Mesh);
          }
      );
    }
  }

  /**
   * Prints the coordinates of the first vertex of a mesh
   */
  public debugFirstMeshCoordinate(mesh: BABYLON.Mesh) {
    if (!mesh || !mesh.getChildren()) {
      return;
    }
    let firstMesh = (mesh.getChildren()[0] as BABYLON.Mesh);
    let vertexData = BABYLON.VertexData.ExtractFromMesh(firstMesh);
    let positions = vertexData.positions;
    let firstVertex = new BABYLON.Vector3(positions[0], positions[1], positions[3]);
    this.updateCoordinateTexture(firstVertex);
  }

  /**
   * Prints the given Vector3
   * @param coordinates
   */
  public updateCoordinateTexture(coordinates: BABYLON.Vector3) {
    if (!coordinates) {
      return;
    }
    this._txtCoordinates.txtX.text = "X: " + coordinates.x;
    this._txtCoordinates.txtY.text = "Y: " + coordinates.y;
    this._txtCoordinates.txtZ.text = "Z: " + coordinates.z;
  }

  private async generateScreens() {
    const numImgs = 8;
    const marginFromSides = 2;
    let numImagesRendered = 0;
    let lastReportedPercentage = -1;
    let limits = {
      posX: { min: -50 + marginFromSides, max: 50 - marginFromSides },
      posY: { min: marginFromSides, max: 20 - marginFromSides },
      posZ: { min: -100 + marginFromSides, max: 100 - marginFromSides },
      angle: { min: 0, max: 360 },
      targetHeight: { min: -0.5, max: 0.5 }
    };
    let datas = [];
    let totalNumberOfImages = Math.pow(8, 5); // 8 images per dimension, 5 dimensions
    for (let posX = limits.posX.min; posX < limits.posX.max; posX += (limits.posX.max - limits.posX.min) / numImgs) {
      for (let posY = limits.posY.min; posY < limits.posY.max; posY += (limits.posY.max - limits.posY.min) / numImgs) {
        for (let posZ = limits.posZ.min; posZ < limits.posZ.max; posZ += (limits.posZ.max - limits.posZ.min) / numImgs) {
          for (let angle = limits.angle.min; angle < limits.angle.max; angle += (limits.angle.max - limits.angle.min) / numImgs) {
            for (let targetHeight = posY + limits.targetHeight.min; targetHeight < posY + limits.targetHeight.max; targetHeight += (limits.targetHeight.max - limits.targetHeight.min) / numImgs) {
              this._camera.position = new BABYLON.Vector3(posX, posY, posZ);
              const radials = angle / Math.PI / 2;
              this._camera.setTarget(new BABYLON.Vector3(posX + Math.sin(radials), targetHeight, posZ + Math.cos(radials)));
              this._scene.render();
              await this.setTimeout(1);
              BABYLON.Tools.CreateScreenshot(this._engine, this._camera, 200, (data) => {
                datas.push({data, posX, posY, posZ, angle, targetHeight});
              });
              numImagesRendered++;
              let currentPercentage = Math.round(numImagesRendered * 100 / totalNumberOfImages);
              if (currentPercentage !== lastReportedPercentage) {
                lastReportedPercentage = currentPercentage;
                console.log(currentPercentage);
              }
            }
          }
        }
      }
    }
    console.log('number of images rendered: ' + numImagesRendered, datas);

    let keys = Object.keys(datas[0]);
    const csvStrings = datas.map((data) => _.map(data, (value) => value).join(',') + '\n');
    csvStrings.unshift(keys.join(',') + '\n');
    const blob = new Blob(csvStrings, {type: "text/csv;charset=utf-8"});
    saveAs(blob, 'images.csv', JSON.stringify(datas));
    // setInterval(() => {
    //   const radials = counter / Math.PI / 2;
    //   this._camera.setTarget(new BABYLON.Vector3(Math.sin(radials), 10, Math.cos(radials)));
    //   counter = (counter + 0.01) % 360;
    // }, 1);
  }

  private async setTimeout(delay: number) {
    return new Promise<void>((resolve) => {
      setTimeout(resolve, delay);
    });
  }
}
